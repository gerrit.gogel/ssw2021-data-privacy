# Data Privacy Project Report

## Table of Contents
1. [Abstract](#abstract)
2. [Project Definition](#project-definition)
3. [Approach](#approach)
4. [Semantic Ressources](#semantic-ressources)
5. [Result](#result)
6. [Future Work](#future-work)
7. [Conclusions](#conclusions)
8. [References](#references)

## Abstract 

In today's world, our data is almost as valuable and popular as a currency. Not just technology companies, but almost all companies are competing to collect and use our data. And they shape their values according to this use. Also, many companies are experimenting with many ways and service models to collect our data. The applications we use almost every day, every hour, are one of these ways to collect our data. We will examine how applications behave about data privacy and usage and their relationship in terms of ontology and semantic web. In our research, we will develop a system over the protégé and examine how the applications process our data and what semantic results will emerge.

## Project Definition

We wanted to look at what data the applications in our daily life use and process from an ontological perspective. The applications and the data they used were appropriately classified through protege. With this classification, the possibility of comparison between applications was created. Which application used the most data, which application used which data? We can achieve these results with sparql. We can interpret these results or take action for future stages and studies. At this stage, we can define our project boundary as representing and comparing applications on Protege. It is also an important point that it is in a very suitable structure for future studies.

## Approach 

First, we started our project by examining the previous works. It was one of our thoughts to make use of or put on past ontologies by the logic of ontology. We have seen the abundance of Spanish-based studies in this field. With these researches and the concepts we gained in the lessons, we started working on applications on protege. We have determined the important points for the project. The ontology structure is based on the privacy details of Apple's App Store. It represents a classifcation of which data is accessed by the application, along with a description of how this data is used. These privacy enables the user to compare applications in regard of data privacy. The goal was to collect the data provided by the app store in an ontology representation, that allows for easier comparison by the use of queries. Although applications and data are at our core, users are also another important point. In line with this concept map, we created subclasses from concepts such as Applications, Data usage, people, user data and data usage request. Applications that we use in our daily life took the examples in the Applications class. Of course, all classes were created separately from each other. In the Data Use class, we have listed examples of the purpose for which data from applications is collected. In addition to Analytics, Appfunctionality and ProductPersonalization, developers, advertising or marketing are some of the usage areas we have discussed. On the other hand, our data is also used for an important profit-making department of the companies. 3rd party advertisements are another area where our data is used. We've also added the Other purposes class for any use other than these topics. Another class we created is DataUseRequest. In this class, we have shown the permission requests for each data to be used for each application as an example. For example, in the Gmail application, data processing permission is requested for user-specific information such as phone number, purchase and research history. Another class is Person, where we can instantiate users. In our last class user information, we can sample all the data collected by the applications. For example, in the ContactInfo class, we can include examples of e-mail address, name, phone number, physical address and OtherUserContactInfo. By classifying the similar ones among the various information obtained during the application, it is facilitated in the use and evaluation of data. Application information, which was previously created manually, has been automated. Along with the application name received from the user, the information used by the application has been added to the class hierarchy accordingly. As new applications are added to the system, our ontology will expand, and connections will be seen more clearly in data usage. A PowerShell script has been implemented, which allows to add any App from the US App Store to the ontology. The script makes use of web-browser automation with Selenium to collect the data from the website.

This graphic illustrates the resulting ontology of our project.

![Data Privacy Ontology](privacypolicy.png "Data Privacy Ontology")

## Semantic Ressources

Even during the formation of our project idea, we benefited from many semantic sources. Such as earlier papers, the meaning of the concept of ontology, semantic studies. Wikidata is one of the most important sources that we benefit from similar examples.
We can examine the personnel data subclasses with the SPARQL Query seen in the figure. Similarly, we aim to make different analyzes with SPARQL Queryler.

![SparqlQuery](sparql.png "PersonelData Classification")

## Result

At the end of the study, we were able to pay attention to the diversity and redundancy of the data collected about us by many applications that we use in our daily lives.  You can also see the SPARQL Query, which shows the data in the personnel data class, in the photo above. When we look at Amazonshopping, CashApp, Disney+, Whatsapp, Facebook, Instagram, Netflix, DoorDash, Element, GAIN, Gmail, Google, GoogleMaps, GoogleMeet, Signal, Snapchat, Spotify, Telegram, TikTok, Youtube, Yemeksepeti and Zoom with a similar method We discovered that the application that uses the most data is Yemeksepeti. Others are Facebook, Instagram, WhatsApp and TikTok. We plan to use sparql in our final assignment to achieve similar results. At this stage, we realized that we could achieve more results with sparql thanks to our professor.

## Future Work

 One of our next works is to improve our database by entering more application examples into our system. As the diversity increases, a better-quality evaluation will be possible. This is directly proportional to the increase in the number of users. Another work area is to maximize the awareness of the user by turning this system into an interactive structure. With the help of a website, the user can enter the name of the application he is curious about and see which data he uses with examples. Or it might discover similar apps that use less data. It is important to know the knowledge and values we give to applications as users.

## Conclusions

As a result, all these research and studies we do are to raise awareness of users about the use of their data and to warn them about privacy issues. Being aware of data privacy, one of the important issues of our future, and being able to manage the situation begin with basic steps. The fact that this situation has become more understandable with the help of ontology once again reveals the importance of the semantic web.

## References
[Apple - App privacy details on the App Store](https://developer.apple.com/app-store/app-privacy-details/)
