﻿# This script imports the data privacy properties of a given App from the US App Store into dataprivacy.owl
# Requires Install-Module Selenium and Firefox browser
# Usage: ./AddAplication.ps1 "https://apps.apple.com/us/app/telegram-messenger/id686449807"

# Creates an individual with given IRI
function CreateIndividual($IRI){
    if(($OWL.GetElementsByTagName("Declaration").GetElementsByTagName("NamedIndividual") | ? IRI -eq $IRI).Count -eq 0){
        $Declaration = $OWL.CreateElement("Declaration",$XmlNs)
        $NamedIndividual = $OWL.CreateElement("NamedIndividual",$XmlNs)
        $NamedIndividual.SetAttribute("IRI",$IRI)
        $Declaration.AppendChild($NamedIndividual)
        $OWL.GetElementsByTagName("Ontology")[0].AppendChild($Declaration)
    }
}

# Assigns an individual to a class
function CreateIndividualClassAssertion($IRI, $Class){
    $Exists = $false
    $OWL.GetElementsByTagName("ClassAssertion") | ForEach-Object {
        if(@($_.GetElementsByTagName("Class") | ? IRI -eq $Class).Count -eq 1 -and @($_.GetElementsByTagName("NamedIndividual") | ? IRI -eq $IRI).Count -eq 1){
            $Exists = $true
        }
    }
    if($Exists -eq $false){
        $ClassAssertion = $OWL.CreateElement("ClassAssertion",$XmlNs)
        $NamedIndividual = $OWL.CreateElement("NamedIndividual",$XmlNs)
        $NamedIndividual.SetAttribute("IRI",$IRI)
        $ClassAssertion.AppendChild($NamedIndividual)
        $IndividualClass = $OWL.CreateElement("Class",$XmlNs)
        $IndividualClass.SetAttribute("IRI",$Class)
        $ClassAssertion.AppendChild($IndividualClass)
        $OWL.GetElementsByTagName("Ontology")[0].AppendChild($ClassAssertion)
    }
}

# Creates an object property assertion between two individiuals
function CreateIndividualObjectPropertyAssertion($ObjectPropertyIRI, $FirstIndividualIRI, $SecondIndividualIRI){
    $Exists = $false
    $OWL.GetElementsByTagName("ObjectPropertyAssertion") | ForEach-Object {
        if(@($_.GetElementsByTagName("ObjectProperty") | ? IRI -eq $ObjectPropertyIRI).Count -eq 1 -and @($_.GetElementsByTagName("NamedIndividual") | ? IRI -eq $FirstIndividualIRI).Count -eq 1 -and @($_.GetElementsByTagName("NamedIndividual") | ? IRI -eq $SecondIndividualIRI).Count -eq 1){
            $Exists = $true
        }
    }
    if($Exists -eq $false){
        $ObjectPropertyAssertion = $OWL.CreateElement("ObjectPropertyAssertion",$XmlNs)
        $ObjectProperty = $OWL.CreateElement("ObjectProperty",$XmlNs)
        $ObjectProperty.SetAttribute("IRI",$ObjectPropertyIRI)
        $ObjectPropertyAssertion.AppendChild($ObjectProperty)
        $FirstIndividual = $OWL.CreateElement("NamedIndividual",$XmlNs)
        $FirstIndividual.SetAttribute("IRI",$FirstIndividualIRI)
        $ObjectPropertyAssertion.AppendChild($FirstIndividual)
        $SecondIndividual = $OWL.CreateElement("NamedIndividual",$XmlNs)
        $SecondIndividual.SetAttribute("IRI",$SecondIndividualIRI)
        $ObjectPropertyAssertion.AppendChild($SecondIndividual)
        $OWL.GetElementsByTagName("Ontology")[0].AppendChild($ObjectPropertyAssertion)
    }
}

# Creates a data property assertion on an individual
function CreateIndividualDataPropertyAssertion($DataPropertyIRI, $IndividualIRI, $LiteralIRI, $LiteralValue){
    $Exists = $false
    $OWL.GetElementsByTagName("DataPropertyAssertion") | ForEach-Object {
        if(@($_.GetElementsByTagName("DataProperty") | ? IRI -eq $DataPropertyIRI).Count -eq 1 -and @($_.GetElementsByTagName("NamedIndividual") | ? IRI -eq $IndividualIRI).Count -eq 1 -and @($_.GetElementsByTagName("Literal") | ? datatypeIRI -eq $LiteralIRI).Count -eq 1){
            $Exists = $true
        }
    }
    if($Exists -eq $false){
        $DataPropertyAssertion = $OWL.CreateElement("DataPropertyAssertion",$XmlNs)
        $DataProperty = $OWL.CreateElement("DataProperty",$XmlNs)
        $DataProperty.SetAttribute("IRI",$DataPropertyIRI)
        $DataPropertyAssertion.AppendChild($DataProperty)
        $Individual = $OWL.CreateElement("NamedIndividual",$XmlNs)
        $Individual.SetAttribute("IRI",$IndividualIRI)
        $DataPropertyAssertion.AppendChild($Individual)
        $Literal = $OWL.CreateElement("Literal",$XmlNs)
        $Literal.SetAttribute("dataTypeIRI",$LiteralIRI)
        $Literal.InnerXml = $LiteralValue
        $DataPropertyAssertion.AppendChild($Literal)
        $OWL.GetElementsByTagName("Ontology")[0].AppendChild($DataPropertyAssertion)
    }
}

# Script starts here
$URI = $args[0]

# Get Data from App Store
$Driver = Start-SeFireFox
Enter-SeUrl $URI -Driver $Driver
$AppTitle = $Driver.Title -replace("\son\sthe\sApp\sStore","")
$AppTitleClean = $AppTitle -replace("\s","") -replace(":.*","") -replace("-.*","")
$AppIRI = "#" + $AppTitleClean
$AppPrivacyDetailsButton = Find-SeElement -Driver $Driver -TagName "div.app-privacy--modal.privacy-type--modal"
Invoke-SeClick -Element $AppPrivacyDetailsButton
sleep 1
$AppPrivacyDetailsText = (Find-SeElement -Driver $Driver -ClassName "we-modal__content__wrapper").Text
$Driver.Dispose()

# Read owl file
[xml]$OWL = Get-Content ($PSScriptRoot + "\privacypolicy.owl")
$XmlNs = $OWL.DocumentElement.NamespaceURI

# Create Application individual
CreateIndividual $AppIRI

# Create Application individual class assertion
CreateIndividualClassAssertion $AppIRI "#Application"

# Convert app store data to owl xml
$DataLinkedToUser = $null
$DataUsedToTrackUser = $null
$DataUse = $null
$Skip = $false
# for each DataUseRequest...
$AppPrivacyDetailsText -split "`r`n" | ForEach-Object {
    if($_ -match "Data Used to Track You"){
        $DataUsedToTrackUser = $true
        $DataLinkedToUser = $true
        $Skip = $true
    }
    elseif($_ -match "Data Linked to You"){
        $DataLinkedToUser = $true
        $DataUsedToTrackUser = $false
        $Skip = $true
    }
    elseif($_ -match "Data Not Linked to You"){
        $DataLinkedToUser = $false
        $DataUsedToTrackUser = $false
        $Skip = $true
    }
    elseif(($DataLinkedToUser -eq $null -and $DataLinkedToTrackUser -eq $null) -or $Skip -eq $true){
        $Skip = $false
        return
    }
    #Skip data type Categories (expect those which are named the same as data types)
    elseif($_ -eq "Contact Info" -or $_ -eq "Health & Fitness" -or $_ -eq "Finincial Info" -or $_ -eq "Location" -or $_ -eq "User Content" -or $_ -eq "Identifiers" -or $_ -eq "Purchases" -or $_ -eq "Usage Data" -or $_ -eq "Diagnostics"  -or $_ -eq "Other Data"){
        return
    }
    elseif($_ -match "Analytics" -or $_ -match "App Functionality" -or $_ -match "Developer’s Advertising or Marketing" -or $_ -match "Other Purposes" -or $_ -match "Product Personalization" -or $_ -match "Third-Party Advertising"){
        $DataUse = "#" + $_ -replace(" ","") -replace("’","") -replace("-","")
    }
    else{
        $DataUseRequestName = $AppIRI + $_ -replace(" ","")
        #Create DataUseRequest individual
        CreateIndividual $DataUseRequestName
        #Create DataUseRequest individual class assertion
        CreateIndividualClassAssertion $DataUseRequestName "#DataUseRequest"
        #Create DataUseRequest individual ObjectPropertyAssertion hasDataUse
        #If DataUsedToTrackUser is true then no DataUse is defined 
        if($DataUsedToTrackUser -eq $false){
            CreateIndividualObjectPropertyAssertion "#hasDataUse" $DataUseRequestName $DataUse
        }
        #Create DataUseRequest individual ObjectPropertyAssertion requestedByApplication
        CreateIndividualObjectPropertyAssertion "#requestedByApplication" $DataUseRequestName $AppIRI
        #Create DataUseRequest individual ObjectPropertyAssertion requestsPersonalData
        CreateIndividualObjectPropertyAssertion "#requestsPersonalData" $DataUseRequestName ("#" + $_ -replace(" ",""))
        #Create DataUseRequest individual DataPropertyAssertion DataLinkedToUser
        CreateIndividualDataPropertyAssertion "#DataLinkedToUser" $DataUseRequestName "http://www.w3.org/2001/XMLSchema#boolean" $DataLinkedToUser.ToString().ToLower()
        #Create DataUseRequest individual DataPropertyAssertion DataUsedToTrackUser
        CreateIndividualDataPropertyAssertion "#DataUsedToTrackUser" $DataUseRequestName "http://www.w3.org/2001/XMLSchema#boolean" $DataUsedToTrackUser.ToString().ToLower()
    }
}

#Save owl
$OWL.Save($PSScriptRoot + "\privacypolicy.owl")
